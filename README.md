# Onboarding Assignment

This assignment is related to REST API's in Node.js

## Prerequisites / Tech Stack

1- NodeJS  
2- Express  
3- Javascript  
4- Unittest: Jest  

## Major Libraries  

1- Express  
2- neat-csv  
3- body-parser  
4- fs  
5- jest  


## How to run the project

After cloning the repository open the project into Visual Studio Code Editor. Run the below mentioned command to run the server on terminal
```node index.js
```  
After executing the command go to the broswer Localhost (http://localhost:9090) 



## Funtionality of API on behalf of simple input
API is used to read data from csv file. User will enter the specific range of dates e.g(startDate, endDate), API will return the total number of record(s)
between range & also first & last object from the resulted range.

## API Result upon success
{  
    "responseCode": 200,  
    "responseMessage": "Success",  
    "response": {  
      "count" : 100,  
      "firstObject": {Object},  
      "lastObject":{Object}  
    }  
}  

## API Result opon failure
{
    "responseCode": 404,  
    "responseMessage": "Failure",  
    "response": {  
      "error": {  
        "message": "<Reason of error>"  
      }  
    }  
}  

## Funtionality of API on behalf of different input
API is used to read data from csv file on behalf of different inputs. User will enter the specific range of dates e.g(startDate, endDate), API will return the total number of record(s)
between range & also first & last object from the resulted range.

## API Result upon success
  "responseCode": 200,
      "responseMessage": "Success",
      "response": [
          {
              "intervalStart": "1483271940",
              "intervalEnd": "1483275540",
              "count": 60,
              "firstObject": {
                  "time": "1483271940",
                  "close": "977.99",
                  "high": "977.99",
                  "low": "977.99",
                  "open": "977.99",
                  "volume": "0"
              },
              "lastObject": {
                  "time": "1483275540",
                  "close": "976.16",
                  "high": "976.16",
                  "low": "976.16",
                  "open": "976.16",
                  "volume": "0"
              }
          }
      ]
    }

## API Result opon failure
  {
      "responseCode": 404,  
      "responseMessage": "Failure",  
      "response": {  
        "error": {  
          "message": "<Reason of error>"  
        }  
      }  
  } 