let {getRequiredArray, getRequiredArrayOnDifferentInput} = require('../services/SearchRecordFromCSVService');
let {getCSVData} = require('../utils/ReadCSV');
const convert = require('../utils/DateTimeConverter');
jest.mock('../utils/ReadCSV');

const testDataV1 = () =>
{
  var object1 = 
  {
    "time" : "1483271940",
    "close" : "968.29",
    "high" : "968.29",
    "low" : "968.29",
    "open" : "968.29",
    "volume" : "3500"
  };

  var object2 = 
  {
    "time" : "1483271960",
    "close" : "968.7",
    "high" : "968.27",
    "low" : "968.29",
    "open" : "968.29",
    "volume" : "3600"
  };
  var object3 = 
  {
    "time" : "1483271980",
    "close" : "968.7",
    "high" : "968.27",
    "low" : "968.29",
    "open" : "968.29",
    "volume" : "3700"
  };
  var object4 = 
  {
    "time" : "1483272300",
    "close" : "968.7",
    "high" : "968.27",
    "low" : "968.29",
    "open" : "968.29",
    "volume" : "3800"
  };
  let data = [];
  data.push(object1, object2, object3, object4);
  return data;
}

const testDataV2 = () =>
{
  let data = 
  [
    {
      
          "time": "1483271940",
          "close": "977.99",
          "high": "977.99",
          "low": "977.99",
          "open": "977.99",
          "volume": "0"
    },
      {
          "time": "1483275540",
          "close": "976.16",
          "high": "976.16",
          "low": "976.16",
          "open": "976.16",
          "volume": "0"
  },
  {
          "time": "1483275540",
          "close": "976.16",
          "high": "976.16",
          "low": "976.16",
          "open": "976.16",
          "volume": "0"
      },
      {
          "time": "1483279140",
          "close": "976.48",
          "high": "976.48",
          "low": "976.48",
          "open": "976.48",
          "volume": "0"
  },
  {
      
          "time": "1483279140",
          "close": "976.48",
          "high": "976.48",
          "low": "976.48",
          "open": "976.48",
          "volume": "0"
      },
      {
          "time": "1483282740",
          "close": "980",
          "high": "980",
          "low": "979.7",
          "open": "979.79",
          "volume": "16000"
      },
      {
          "time": "1483282740",
          "close": "980",
          "high": "980",
          "low": "979.7",
          "open": "979.79",
          "volume": "16000"
      },
      {
          "time": "1483286340",
          "close": "981.72",
          "high": "981.83",
          "low": "981.07",
          "open": "981.75",
          "volume": "10000"
  },
  {
          "time": "1483286340",
          "close": "981.72",
          "high": "981.83",
          "low": "981.07",
          "open": "981.75",
          "volume": "10000"
      },
      {
          "time": "1483289940",
          "close": "1000.96",
          "high": "1003",
          "low": "1000.6",
          "open": "1000",
          "volume": "20600"
      },
  {
      
          "time": "1483289940",
          "close": "1000.96",
          "high": "1003",
          "low": "1000.6",
          "open": "1000",
          "volume": "20600"
      },
       {
          "time": "1483293540",
          "close": "997.15",
          "high": "997.15",
          "low": "996.67",
          "open": "996.7",
          "volume": "6133"
  },
  {
          "time": "1483293540",
          "close": "997.15",
          "high": "997.15",
          "low": "996.67",
          "open": "996.7",
          "volume": "6133"
      },
      {
          "time": "1483297140",
          "close": "1002",
          "high": "1002",
          "low": "1002",
          "open": "1002",
          "volume": "0"
  },
  {
          "time": "1483297140",
          "close": "1002",
          "high": "1002",
          "low": "1002",
          "open": "1002",
          "volume": "0"
      },
      {
          "time": "1483300740",
          "close": "998.5",
          "high": "998.92",
          "low": "998.5",
          "open": "999.08",
          "volume": "51000"
  },
  {
      
          "time": "1483300740",
          "close": "998.5",
          "high": "998.92",
          "low": "998.5",
          "open": "999.08",
          "volume": "51000"
      },
     {
          "time": "1483304340",
          "close": "1002.44",
          "high": "1002.45",
          "low": "1002.15",
          "open": "1002.39",
          "volume": "100901"
  },
  {
          "time": "1483304340",
          "close": "1002.44",
          "high": "1002.45",
          "low": "1002.15",
          "open": "1002.39",
          "volume": "100901"
      },
      {
          "time": "1483307940",
          "close": "1005.04",
          "high": "1005.04",
          "low": "1005.03",
          "open": "1005",
          "volume": "1000"
  },
  {

          "time": "1483307940",
          "close": "1005.04",
          "high": "1005.04",
          "low": "1005.03",
          "open": "1005",
          "volume": "1000"
      },
 {
          "time": "1483311540",
          "close": "1004.35",
          "high": "1004.35",
          "low": "1004.35",
          "open": "1004.83",
          "volume": "5000"
  },
  {
          "time": "1483311540",
          "close": "1004.35",
          "high": "1004.35",
          "low": "1004.35",
          "open": "1004.83",
          "volume": "5000"
      },
      {
          "time": "1483315140",
          "close": "1001.15",
          "high": "1001.16",
          "low": "1001.15",
          "open": "1002.16",
          "volume": "10000"
  },
  {
          "time": "1483315140",
          "close": "1001.15",
          "high": "1001.16",
          "low": "1001.15",
          "open": "1002.16",
          "volume": "10000"
      },
      {
          "time": "1483318740",
          "close": "996.95",
          "high": "996.95",
          "low": "996.95",
          "open": "996.95",
          "volume": "0"
  },
  {
          "time": "1483318740",
          "close": "996.95",
          "high": "996.95",
          "low": "996.95",
          "open": "996.95",
          "volume": "0"
      },
      {
          "time": "1483322340",
          "close": "1000.29",
          "high": "1001",
          "low": "1000.29",
          "open": "1001.49",
          "volume": "4235"
  },
  {
          "time": "1483322340",
          "close": "1000.29",
          "high": "1001",
          "low": "1000.29",
          "open": "1001.49",
          "volume": "4235"
      },
      {
          "time": "1483325940",
          "close": "1012.07",
          "high": "1013.19",
          "low": "1012.04",
          "open": "1012.5",
          "volume": "2033"
  },
  {
          "time": "1483325940",
          "close": "1012.07",
          "high": "1013.19",
          "low": "1012.04",
          "open": "1012.5",
          "volume": "2033"
      },
      {
          "time": "1483329540",
          "close": "1006.98",
          "high": "1008.01",
          "low": "1006.98",
          "open": "1008.01",
          "volume": "108891"
  },
 {
          "time": "1483329540",
          "close": "1006.98",
          "high": "1008.01",
          "low": "1006.98",
          "open": "1008.01",
          "volume": "108891"
      },{
          "time": "1483333140",
          "close": "1011.54",
          "high": "1011.54",
          "low": "1011.54",
          "open": "1011.2",
          "volume": "200"
  },
  {
          "time": "1483333140",
          "close": "1011.54",
          "high": "1011.54",
          "low": "1011.54",
          "open": "1011.2",
          "volume": "200"
      },
      {
          "time": "1483336740",
          "close": "1011.7",
          "high": "1011.7",
          "low": "1011.69",
          "open": "1012.02",
          "volume": "54852"
  },
{
          "time": "1483336740",
          "close": "1011.7",
          "high": "1011.7",
          "low": "1011.69",
          "open": "1012.02",
          "volume": "54852"
      },
{
          "time": "1483340340",
          "close": "1014.82",
          "high": "1014.82",
          "low": "1014.82",
          "open": "1015.14",
          "volume": "30"
  },
 {
          "time": "1483340340",
          "close": "1014.82",
          "high": "1014.82",
          "low": "1014.82",
          "open": "1015.14",
          "volume": "30"
      },
      {
          "time": "1483343940",
          "close": "1020.11",
          "high": "1021.15",
          "low": "1020.1",
          "open": "1022.54",
          "volume": "121352"
  },
  {
          "time": "1483343940",
          "close": "1020.11",
          "high": "1021.15",
          "low": "1020.1",
          "open": "1022.54",
          "volume": "121352"
      },
     {
          "time": "1483347540",
          "close": "1016.3",
          "high": "1016.3",
          "low": "1016.29",
          "open": "1016.17",
          "volume": "33000"
  },
  {
      
          "time": "1483347540",
          "close": "1016.3",
          "high": "1016.3",
          "low": "1016.29",
          "open": "1016.17",
          "volume": "33000"
      },
      {
          "time": "1483351140",
          "close": "1017.37",
          "high": "1017.37",
          "low": "1017.37",
          "open": "1017.37",
          "volume": "0"
  },
  {
          "time": "1483351140",
          "close": "1017.37",
          "high": "1017.37",
          "low": "1017.37",
          "open": "1017.37",
          "volume": "0"
      },
       {
          "time": "1483354740",
          "close": "1020.06",
          "high": "1020.06",
          "low": "1020.06",
          "open": "1020.06",
          "volume": "0"
  },
{
          "time": "1483354740",
          "close": "1020.06",
          "high": "1020.06",
          "low": "1020.06",
          "open": "1020.06",
          "volume": "0"
      },
      {
          "time": "1483358340",
          "close": "1028.46",
          "high": "1028.46",
          "low": "1028.46",
          "open": "1029.76",
          "volume": "500"
  }
  ];
  return data;
}

describe('Get Records From CSV', () =>
{
    it('Graph Data V1', async () =>
    {
      const start = '2017-01-01T16:59:00';
      const end = '2017-01-01T17:05:00';
      const start1 = convert.convertTimeStampToEpoch(start);
      const end1 = convert.convertTimeStampToEpoch(end);
      getCSVData.mockImplementation(() => Promise.resolve(testDataV1()));
      const result = await getRequiredArray(start1, end1);

      expect(result).not.toBeNull();

      expect(result.length).toBeGreaterThan(0);

      expect(Array.isArray(result)).toEqual(true);

      expect(Number(result.length)).toEqual(3);

      expect(Number(result[2])).toEqual(3);
    });


    it('Graph Data V2', async () =>
    {
      const start = '2017-01-01T16:59:00';
      const end = '2017-01-02T16:59:00';
      getCSVData.mockImplementation(() => Promise.resolve(testDataV2()));
      const result = await getRequiredArrayOnDifferentInput(start, end);

      expect(result).not.toBeNull();

      expect(result.length).toBeGreaterThan(0);

      expect(Array.isArray(result)).toEqual(true);

      expect(Number(result.length)).toEqual(24);

      expect(Number(result[0].count)).toEqual(2);
    });
});