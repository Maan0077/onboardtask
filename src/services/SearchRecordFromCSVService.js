const readCSV = require('../utils/ReadCSV');
const dateTimeConverter = require('../utils/DateTimeConverter');

/**
 * @generator Muhammad Yaseen
 * @param {*} array 
 * @param {*} start 
 * @param {*} end 
 * this method is used to extract the required data from csv upon giving range
 */
const getRequiredArray = async (start, end)  =>
{
    let manipulationArray = [];
    manipulationArray = await readCSV.getCSVData();
    try
    {
        return searchRecordsFromCSV(manipulationArray, start, end);
    } 
    catch (error)
    {
        console.log(error);
        return null;
    }
}

/**
 * @generator Muhammad Yaseen
 * @param {*} start
 * @param {*} end 
 * this method is used to get the data from csv on behalf of different inputs daily, weekly, monthly & annualy
 */
const getRequiredArrayOnDifferentInput = async (date1, date2)  =>
{
    const start = dateTimeConverter.convertTimeStampToEpoch(date1);
    const end = dateTimeConverter.convertTimeStampToEpoch(date2);
    const startDate = new Date(start * 1000);
    const endDate = new Date(end * 1000);
    let hours = dateTimeConverter.convertGivenRangeintoHours(startDate, endDate);

    try 
    {
      let finalArray = [];
      const counter = dateTimeConverter.getDifference(hours);
      const manipulationArray = await readCSV.getCSVData();
      if (counter) 
      {
          for (let temp = 0; temp < counter; temp++) 
          {
              let newStart, newEnd;
              if (counter == 24)
              {
                newStart = start + (60 * 60 * temp);
                newEnd = start + (60 * 60 * (temp + 1));
              }
              else 
              {
                newStart = start + 1440 * 60 * temp;
                newEnd = start + 1440 * 60 * (temp + 1);
              }

              const resultedArray = searchRecordsFromCSV(manipulationArray, newStart, newEnd);
              const toJson = 
              {
                  intervalStart: resultedArray[0].time,
                  intervalEnd: resultedArray[1].time,
                  count: resultedArray[2],
                  firstObject: resultedArray[0],
                  lastObject: resultedArray[1]
              }
              finalArray.push(toJson);
          }
      }
      else 
      {
          return null;
      }
      return finalArray;
  }
  catch (error) 
  {
      console.log(error);
      return null;
  }
}

/**
 * @generator Muhammad Yaseen
 * @param {*} manipulationArray 
 * @param {*} start 
 * @param {*} end 
 * this method is used to filter the data from array of records on behalf of start & end 
 * And return an array
 */
const searchRecordsFromCSV = (manipulationArray, start, end) =>
{
    let requiredArray = [];
    if(manipulationArray.length > 0)
    {
        let startIndex = 0;
        let lastIndex = 0;
        let firstObject;
        let lastObject;
        for(let temp = 0; temp < manipulationArray.length; temp++)
        {
            if(manipulationArray[temp].time == start)
            {
                firstObject = manipulationArray[temp];
                startIndex = temp;
            }
            else if(manipulationArray[temp].time == end)
            {
                lastObject = manipulationArray[temp];
                lastIndex = temp;
            }
        }
        const count = lastIndex - startIndex;
        requiredArray.push(firstObject, lastObject, count);
        if(requiredArray.length  >= 3)
        {
            return requiredArray;
        }
        else
        {
            return null;
        }
    }
    else
        return null;
}

//this statement are used to export the methods outside the file
module.exports = {getRequiredArray, getRequiredArrayOnDifferentInput};