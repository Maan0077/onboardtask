/**
 * @generator Muhammad Yaseen
 * @param {*} array 
 * use this method if record(s) is/are found successfully from csv
 */
const onSuccessJson = (array) =>
{
    let jsonBody;
    if(array.length > 0)
    {
        jsonBody = 
        {
          "responseCode": 200,
          "responseMessage": "Success",
          "response": 
          {
            "count" : array[2],
            "firstObject": array[0],
            "lastObject": array[1]
          }
        }
    }
    return jsonBody;
}

/**
 * @generator Muhammad Yaseen
 * @param {*} startDate 
 * @param {*} endDate 
 * @param {*} array 
 * @details this method is used to pass the json response with start & end date
 */
const onSuccessJsonOne = (array) =>
{
    let jsonBody;
    if(array.length > 0)
    {
      jsonBody = 
      {
          "responseCode": 200,
          "responseMessage": "Success",
          "response": array
      }
    }
    return jsonBody;
}

/**
 * @generator Muhammad Yaseen
 * use this method if records is not found from csv
 */
const onFailureJson = () =>
{
    let jsonBody = 
    {
        "responseCode": 404,
        "responseMessage": "Failure",
        "response": 
        {
            "error": 
            {
                "message": "Not found"
            }
        }
    }
      return jsonBody;
}

module.exports = {onFailureJson, onSuccessJson, onSuccessJsonOne};