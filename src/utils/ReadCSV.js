const fs = require('fs');
const neatCsv = require('neat-csv');

/**
 * @generator Muhammad Yaseen
 * this method is used to read the csv records if successfully reads the records than return resolve if any 
 * error occurd return reject
 */
const readCSV = () =>
{
    return new Promise ((resolve, reject) =>
    {
        let csvRecords = [];
        fs.readFile('../bitcoin.csv', async (err, data) =>
        {
            if (err)
            {
                console.error(err);
                reject;
            }
            console.log("Data loaded");
            csvRecords = await neatCsv(data);
            resolve(csvRecords);
        });
    });
}

/**
 * @generator Muhammad Yaseen
 * this method is used to get the data of csv
 */
const getCSVData = () =>
{
    const array = readCSV();
    return array;
}


module.exports = {readCSV, getCSVData};