
/**
 * @generator Muhammad Yaseen
 * @param {*} dateTimeStamp 
 * convert the dataTimeStamp into epoch 
 */
const convertTimeStampToEpoch = (dateTimeStamp) =>
{
    const convertedDate = Math.round((new Date(dateTimeStamp)).getTime() / 1000);
    return convertedDate;
}

/**
 * @generator Muhammad Yaseen
 * @param {} start 
 * @param {*} end 
 * this method is used to convert the dataTime format to seconds
 */
const convertGivenRangeIntoSeconds = (start, end) =>
{
    let millisecondDifference = end - start;
    let secDifference = Math.floor(millisecondDifference / 1000);
    return secDifference;
}

/**
 * @generator Muhammad Yaseen
 * @param {} start 
 * @param {*} end 
 * this method is used to convert the dataTime format to mintus
 */
const convertGivenRangeIntoMintus = (start, end) =>
{
    let seconds = convertGivenRangeIntoSeconds(start, end);
    let minutesDifference = Math.floor(seconds / 60);
    return minutesDifference;
}


/**
 * @generator Muhammad Yaseen
 * @param {} start 
 * @param {*} end 
 * this method is used to convert the dataTime format to hours
 */
const convertGivenRangeintoHours = (start, end) =>
{
  let minutes = convertGivenRangeIntoMintus(start, end);
  let hoursDifference = Math.floor(minutes / 60);
  return hoursDifference;
}

/**
 * @generator Muhammad Yaseen
 * @param {} start 
 * @param {*} end 
 * this method is used to measure the difference
 */
const getDifference = (hours) => 
{
  const result = [24, 168, 672, 696, 728, 744, 8760, 8784];
  return result.includes(hours) ? hours == 24 ? 24 : Math.round(hours / 24) : undefined
}

module.exports = {convertTimeStampToEpoch, convertGivenRangeintoHours, getDifference};