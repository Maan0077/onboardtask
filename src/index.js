const express = require('express');
const bodyParser = require('body-parser');
const searchRecordFromCSV = require('./services/SearchRecordFromCSVService');
const responseBody = require('./utils/ResponseBody');
const dateTimeConverter = require('./utils/DateTimeConverter');


const app = express();
const port = process.env.PORT || 9090;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});

//post API send the data into body to get data from CSV
app.post('/api/v1/graphData', async function(req, res)
{
    const rangeStart = req.body.rangeStart;
    const rangeEnd = req.body.rangeEnd;
    const convertedStartDate = dateTimeConverter.convertTimeStampToEpoch(rangeStart);
    const convertedEndDate = dateTimeConverter.convertTimeStampToEpoch(rangeEnd);
    const resultedArray = await searchRecordFromCSV.getRequiredArray(convertedStartDate, convertedEndDate);
    if(resultedArray != null && resultedArray.length > 0)
    {
        res.status(200).json(responseBody.onSuccessJson(resultedArray));
    }
    else
    {
        res.status(200).json(responseBody.onFailureJson());
    }
});

//post API send the data into body to get data from CSV on behalf of different inputs
app.post('/api/v2/graphData', async (req, res) => 
{
    const rangeStart = req.body.rangeStart;
    const rangeEnd = req.body.rangeEnd;
    const resultedArray = await searchRecordFromCSV.getRequiredArrayOnDifferentInput(rangeStart, rangeEnd);
    if(resultedArray != null && resultedArray.length > 0)
    {
        res.status(200).json(responseBody.onSuccessJsonOne(resultedArray));
    }
    else
    {
        res.status(200).json(responseBody.onFailureJson());
    }
});