FROM node:12.18.3-stretch
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD [ "npm", "start" ]


#BUILD THE IMAGE
#docker build -t node_docker .

#RUN THE DOCKER CONTAINER
#docker run -it -p 9000:3000 node_docker

#RUN DOCKER CONTAINER AS A BACKGROUND
#docker run -d -p 9000:3000 node_docker

#SEE THE DETAILS
#docker ps
